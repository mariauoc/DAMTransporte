/*
 * Herencia
 */
package damtransport;

import entities.Camion;
import entities.Coche;
import entities.Furgoneta;
import entities.Microbus;
import entities.Vehiculo;
import entities.VehiculoPersona;
import excepciones.ExceptionTransporte;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DAM
 */
public class DamTransport {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            List<Vehiculo> flota = new ArrayList();
            // Creamos un vehiculo de cada tipo y lo incorporamos
            // al ArrayList
            Coche c = new Coche(0, "1111AAA");
            Microbus m = new Microbus(10, "2222BBB");
            Camion ca = new Camion(4000, "3333CCC");
            Furgoneta f = new Furgoneta(true, 2000, "4444DDD");
            // lo añadimos a la lista
            flota.add(c);
            flota.add(m);
            flota.add(f);
            flota.add(ca);
            // Recorremos la lista y mostramos el alquiler para 8 días
            // de cada uno
            for (Vehiculo v : flota) {
                System.out.println(v.getMatricula() + " "+ v.calcularAlquiler(8));
                if (v instanceof Furgoneta) {
                    Furgoneta aux = (Furgoneta) v;
                    System.out.println("Es una furgoneta: "+aux.isRefrigeracion());
                }
            }
        } catch (ExceptionTransporte ex) {
            System.out.println(ex.getMessage());
        }
    }
    
}
