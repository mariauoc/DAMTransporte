/*
 * Ejemplo con HashMap
 */
package damtransport;

import entities.Camion;
import entities.Coche;
import entities.Vehiculo;
import excepciones.ExceptionTransporte;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mfontana
 */
public class TransportHash {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // Definición de HashMap <tipo_clave, tipo_objeto_queseguarda>
            HashMap<String, Vehiculo> flota = new HashMap<>();
            Coche c = new Coche(5, "1234ABC");
            // Añadimos coche al hashmap indicandole que la clave es la matrícula
            flota.put(c.getMatricula(), c);
            Camion ca = new Camion(3000, "8888FFF");
            flota.put(ca.getMatricula(), ca);
            // Si queremos saber si ya existe una matrícula igual
            Coche s = new Coche(2, "1234ABC");
            if (flota.containsKey(s.getMatricula())) {
                System.out.println("Ya existe un vehículo con esa matrícula");
            } else {
                flota.put(s.getMatricula(), s);
            }
            for (String matricula : flota.keySet()) {
                Vehiculo v = flota.get(matricula);
                System.out.println(v);
//                System.out.println(flota.get(matricula));
            }
            
            
            
        } catch (ExceptionTransporte ex) {
            System.out.println(ex.getMessage());
        }
    }
    
}
