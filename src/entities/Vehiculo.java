/*
 * Vehiculo , clase padre abstracta
 */
package entities;

/**
 *
 * @author DAM
 */
public abstract class Vehiculo {
    
    private String matricula;

    public Vehiculo(String matricula) {
        this.matricula = matricula;
    }
    
    // Al indicar este método en Vehiculo estamos obligando a sus hijos a
    // implementarlo o a seguir siendo abstractas
    public double calcularAlquiler(int dias) {
        return 50 * dias;
    }
    
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public String toString() {
        return "Matricula=" + matricula ;
    }
    
    

}
