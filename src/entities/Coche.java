/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import excepciones.ExceptionTransporte;

/**
 *
 * @author mfontana
 */
public class Coche extends VehiculoPersona {

    public Coche(int plazas, String matricula) throws ExceptionTransporte {
        super(plazas, matricula);
    }

    @Override
    public double calcularAlquiler(int dias) {
        return super.calcularAlquiler(dias) + 1.5 * getPlazas() * dias;
    }

    @Override
    public String toString() {
        return "Coche{" + super.toString() + '}';
    }
    
    
    
}
