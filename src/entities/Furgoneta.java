/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author mfontana
 */
public class Furgoneta extends VehiculoCarga {
    
    private boolean refrigeracion;

    public Furgoneta(boolean refrigeracion, int pma, String matricula) {
        super(pma, matricula);
        this.refrigeracion = refrigeracion;
    }
    

    public boolean isRefrigeracion() {
        return refrigeracion;
    }

    public void setRefrigeracion(boolean refrigeracion) {
        this.refrigeracion = refrigeracion;
    }

    @Override
    public double calcularAlquiler(int dias) {
        double precio = super.calcularAlquiler(dias);
        if (refrigeracion) {
            precio += 10 * dias;
        }
        return precio;
    }

}
