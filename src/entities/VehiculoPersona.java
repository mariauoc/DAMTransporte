/*
 * Transporte de personas
 */
package entities;

import excepciones.ExceptionTransporte;

/**
 *
 * @author DAM
 */
public abstract class VehiculoPersona extends Vehiculo {

    private int plazas;

    public VehiculoPersona(int plazas, String matricula) throws ExceptionTransporte {
        super(matricula);
        if (plazas > 0) {
            this.plazas = plazas;
        } else {
            throw new ExceptionTransporte("ERROR: Nº de plazas incorrecto");
        }
    }

    public int getPlazas() {
        return plazas;
    }

    public void setPlazas(int plazas) {
        this.plazas = plazas;
    }

    @Override
    public String toString() {
        return  super.toString() + " plazas=" + plazas;
    }

    
    
}
