/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author mfontana
 */
public class Camion extends VehiculoCarga {

    public Camion(int pma, String matricula) {
        super(pma, matricula);
    }

    @Override
    public double calcularAlquiler(int dias) {
        return super.calcularAlquiler(dias) + 40;
    }

    @Override
    public String toString() {
        return "Camion{" + super.toString() +'}';
    }
    
    
    
    
}
